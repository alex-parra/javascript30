import { drums } from './drums';

/**
 * Utility to find the closest element with the specified className
 */
const closestEl = (el, className) => {
  if (el.classList.contains(className)) return el;
  if (el.parentElement) return closestEl(el.parentElement, className);
};

(function() {
  const app = document.getElementById('app');
  const audioFiles = require('./sounds/*.wav');

  const drumKeys = [];
  drums.forEach(drum => {
    drum.keyEl = document.createElement('button');
    drum.keyEl.setAttribute('data-key', drum.key);
    drum.keyEl.className = 'drum';

    const keyLbl = document.createElement('kbd');
    keyLbl.textContent = drum.key;
    drum.keyEl.appendChild(keyLbl);

    const soundLbl = document.createElement('small');
    soundLbl.textContent = drum.lbl;
    drum.keyEl.appendChild(soundLbl);

    drum.audioEl = document.createElement('audio');
    drum.audioEl.setAttribute('src', audioFiles[drum.soundFile.replace('.wav', '')]);
    drum.keyEl.appendChild(drum.audioEl);

    app.appendChild(drum.keyEl);
    drumKeys.push(drum.key);
  });

  /**
   * Play the drum
   */
  const playDrum = key => {
    const drum = drums.find(d => d.key === key);
    if (!drum) return;

    drum.audioEl.currentTime = 0;
    drum.audioEl.play();
    drum.keyEl.classList.add('playing');
  };

  /**
   * Play drum with click
   */
  document.addEventListener('click', ev => {
    const drumButton = closestEl(ev.target, 'drum');
    if (!drumButton) return;
    ev.preventDefault();
    playDrum(drumButton.dataset.key);
  });

  /**
   * Play drums with keyboard
   */
  document.addEventListener('keydown', ev => {
    const key = ev.key.toLowerCase();
    if (drumKeys.includes(key) !== true) return;

    ev.preventDefault();
    playDrum(key);
  });

  /**
   * When the 'playing' transition ends this event gets fired.
   */
  document.addEventListener('transitionend', ev => {
    const { target } = ev;
    if (target instanceof HTMLElement) target.classList.remove('playing');
  });
})();
