const drums = [
  { key: 'a', lbl: 'Clap', soundFile: 'clap.wav', keyEl: undefined, audioEl: undefined },
  { key: 's', lbl: 'Hihat', soundFile: 'hihat.wav', keyEl: undefined, audioEl: undefined },
  { key: 'd', lbl: 'Kick', soundFile: 'kick.wav', keyEl: undefined, audioEl: undefined },
  { key: 'f', lbl: 'Openhat', soundFile: 'openhat.wav', keyEl: undefined, audioEl: undefined },
  { key: 'g', lbl: 'Boom', soundFile: 'boom.wav', keyEl: undefined, audioEl: undefined },
  { key: 'h', lbl: 'Ride', soundFile: 'ride.wav', keyEl: undefined, audioEl: undefined },
  { key: 'j', lbl: 'Snare', soundFile: 'snare.wav', keyEl: undefined, audioEl: undefined },
  { key: 'k', lbl: 'Tom', soundFile: 'tom.wav', keyEl: undefined, audioEl: undefined },
  { key: 'l', lbl: 'Tink', soundFile: 'tink.wav', keyEl: undefined, audioEl: undefined },
];

export { drums };
