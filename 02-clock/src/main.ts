(function() {
  const app = document.getElementById('app');

  const clock = document.createElement('div');
  clock.classList.add('clock');

  const hourHand = document.createElement('div');
  hourHand.classList.add('hand', 'hour');
  const minuteHand = document.createElement('div');
  minuteHand.classList.add('hand', 'minute');
  const secondHand = document.createElement('div');
  secondHand.classList.add('hand', 'second');

  clock.appendChild(hourHand);
  clock.appendChild(minuteHand);
  clock.appendChild(secondHand);

  app.appendChild(clock);

  setInterval(() => {
    const now = new Date();
    const hours = now.getHours();
    const minutes = now.getMinutes();
    const seconds = now.getSeconds();

    const hoursDeg = hours * 360/12;
    const minutesDeg = minutes * 360/60;
    const secondsDeg = seconds * 360/60;

    hourHand.style.transform = `rotate(${hoursDeg}deg)`;
    minuteHand.style.transform = `rotate(${minutesDeg}deg)`;
    secondHand.style.transform = `rotate(${secondsDeg}deg)`;

  }, 1000);

})();
